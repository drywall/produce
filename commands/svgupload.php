<?php
/**
 * Deprecated command to allow for SVG uploads.
 *
 * @package Produce
 */

/**
 * Deprecated command to allow for SVG uploads.
 *
 * @extends Crate_Produce
 */
class SVG_Upload_Command extends Crate_Produce {

	/**
	 * DEPRECATED. Used to add svg-upload.inc to Crate's /inc/ directory to allow SVG uploads.
	 *
	 * ## OPTIONS
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 */
	public function __invoke( $args, $assoc_args ) {

		// Probably best to be courteous at the end.
		WP_CLI::error( 'This command is deprecated. Use `wp plugin install svg-support --activate` to install a plugin instead.' );

	}
}

WP_CLI::add_command( 'produce svgupload', 'SVG_Upload_Command' );
