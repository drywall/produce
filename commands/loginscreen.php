<?php
/**
 * Tailors the WordPress login screen with custom logo and background color.
 *
 * @package Produce
 */

/**
 * Tailors the WordPress login screen with custom logo and background color.
 *
 * @extends Crate_Produce
 */
class Login_Screen_Command extends Crate_Produce {

	/**
	 * Tailors the WordPress login screen with custom logo and background color.
	 *
	 * ## OPTIONS
	 *
	 * <logopath>
	 * : The path WITHIN CRATE at which the logo is located.
	 *
	 * [--width=<dimension>]
	 * : The width at which to display the logo image.
	 *
	 * [--height=<dimension>]
	 * : The height at which to display the logo image.
	 *
	 * [--bg=<background>]
	 * : The CSS background property to use on the login page.
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce loginscreen images/login-logo.jpg --force
	 *
	 *     wp produce loginscreen images/login_2x.png --width=300px --height=auto --bg=#CCC
	 */
	public function __invoke( $args, $assoc_args ) {

		// Check to see if the file exists.
		$file_with_path = $this->crate_dir . '/' . $args[0];
		if ( ! file_exists( $file_with_path ) ) {
			WP_CLI::error( 'Could not find ' . $file_with_path );
		}

		// Check to see if the file can be processed.
		$image_info = getimagesize( $file_with_path );

		// if it can't, ask the user if they want to proceed anyway (because it's an SVG or something)
		if ( false === $image_info ) {
			WP_CLI::warning( 'The file ' . $file_with_path . ' does not appear to be a supported image type (jpg/gif/png).' );
			$continue = self::prompt(
				'Do you wish to continue?',
				array( 'Y', 'N' ),
				'N'
			);
			// Quit if the user wants to.
			if ( 'N' === $continue ) {
				WP_CLI::error( 'Abandoning ship' );
			} else {
				// Populate the image_info array with some basic dummy data so the remaining steps work.
				$image_info = array( '200', '200' );
			}
		}

		$aspect_ratio   = $image_info[1] / $image_info[0];

		// Get the width
		if ( array_key_exists( 'width', $assoc_args ) ) {
			$width = $assoc_args['width'];
		} else {
			$width = self::prompt(
				'Enter logo image width (recommended max 320px)',
				false,
				$image_info[0] . 'px'
			);
		}

		// Get the height
		if ( array_key_exists( 'height', $assoc_args ) ) {
			$height = $assoc_args['height'];
		} else {
			$width_no_px = str_replace( 'px', '', $width );
			$height_default = ctype_digit( $width_no_px ) ? (int) $width_no_px * $aspect_ratio : $image_info[1];
			$height = self::prompt(
				'Enter logo image height',
				false,
				$height_default . 'px'
			);
		}

		// Get the background
		if ( array_key_exists( 'bg', $assoc_args ) ) {
			$height = $assoc_args['bg'];
		} else {
			$bg = self::prompt(
				'Enter CSS background value',
				false,
				'#f1f1f1'
			);
		}

		// Put t all together
		$data = array(
			'file'   => str_replace( home_url(), '', get_theme_root_uri() . '/crate/' . $args[0] ),
			'width'  => $width,
			'height' => $height,
			'bg'     => $bg,
		);
		$this->enqueue( 'loginscreen.mustache', '/inc/login-screen.php', $data );

		// Once everything is enqueued, produce it.
		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Probably best to be courteous at the end.
		WP_CLI::success( 'Successfully generated login screen customization.' );

	}
}

WP_CLI::add_command( 'produce loginscreen', 'Login_Screen_Command' );
