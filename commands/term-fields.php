<?php
/**
 * Implements common ACF fields for taxonomy terms
 *
 * @package Produce
 */

/**
 * This command initializes four ACF fields for terms in the specified taxonomies:
 * Description (WYSIWYG replacement for native WP field), Featured Image, Icon and Color
 *
 * @extends Crate_Produce
 */
class Term_Fields_Command extends Crate_Produce {

	/**
	 * Creates four custom fields for taxonomy terms.
	 *
	 * ## OPTIONS
	 *
	 * [--taxonomies=<taxonomy,taxonomy>]
	 * : Comma-separated list of taxonomies the fields should be available on, e.g. category,post_tag
	 *
	 * [--icon-path=<path>]
	 * : Path to SVG directory in Crate containing term icon options (default: /_src/img/svg/)
	 *
	 * [--skip-template]
	 * : Don't bother injecting archive.php template that demonstrates basic field value retrieval.
	 *
	 * [--force]
	 * : Force overwrite of existing files, if any.
	 *
	 * ## EXAMPLES
	 *
	 *     wp produce term-fields --skip-template
	 *
	 *     wp produce term-fields --taxonomies=category,article-type
	 */
	public function __invoke( $args, $assoc_args ) {

		// Defaults
		$skip_template = false;
		$icon_path     = '/_src/img/svg/';

		if ( isset( $assoc_args['skip-template'] ) ) {
			$skip_template = true;
		}

		// Set the taxonomies, if specified
		if ( array_key_exists( 'taxonomies', $assoc_args ) ) {

			$taxonomies = explode( ',', $assoc_args['taxonomies'] );

		// Prompt to ask for the taxonomies otherwise
		} else {

			$registered_taxonomies = get_taxonomies(
				array( 'show_ui' => true ),
				'names'
			);

			$taxonomies = self::prompt(
				'Enter comma-separated list of taxonomies to apply fields to. Current registered: ' . implode( ',' , $registered_taxonomies ),
				null,
				'category'
			);

			$taxonomies = explode( ',', $taxonomies );

		}//end if

		// Set the icon path, if specified
		if ( array_key_exists( 'icon-path', $assoc_args ) ) {

			$icon_path = $assoc_args['icon-path'];

		// Prompt to ask for the taxonomies
		} else {

			$icon_path = self::prompt(
				'Enter (crate-relative) path to the directory with SVG icons for the term icon field: ',
				null,
				$icon_path
			);

		}

		/**
		 * Line up the files!
		 */

		// Process the main file that registers the ACF fields
		$this->enqueue( 'term-fields.json.mustache', '/fieldgroups/term-fields.json', $this->mustache_array( $taxonomies, 'taxonomies', 'taxonomy' ) );

		// Process the file that filters the ACF fields and whatnot
		$this->enqueue( 'term-fields.inc.mustache', '/inc/term-fields.inc', array( 'iconpath' => $icon_path ) );

		// Process the archive template, and not into /template-parts/
		if ( ! $skip_template ) {
			$this->enqueue( 'archive.php', '/archive.php' );
		}

		// @todo write and **optionally** include archive-article.php
		$do_overwrite   = isset( $assoc_args['force'] ) ? true : false;
		$output_results = isset( $assoc_args['quiet'] ) ? false : true;
		$this->produce( $do_overwrite, $output_results );

		// Probably best to be courteous at the end.
		WP_CLI::success( 'Term fields implemented.' );

	}
}

WP_CLI::add_command( 'produce term-fields', 'Term_Fields_Command' );
