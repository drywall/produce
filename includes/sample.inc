<?php

/**
 * An include file will probably implement some filters
 */

function filter_title( $title ) {
	return "Produce sample is filtering the title.";
}

add_filter( 'the_title', 'filter_title' );