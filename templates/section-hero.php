<?php
/**
 * The template for displaying a hero section
 */

// Get the ID of the background image
$image_id  = get_sub_field( 'hero_background_image' );

// Get the URLs of the image
$image = wp_get_attachment_image_src( $image_id, apply_filters( 'crate_hero_image_size', 'width=1800&height=9999&crop=1' ) );

// Note: mobile-image is passed as a data-mobile-image attribute and not in CSS. To use it, you'll need to write some JS.
$mobile_image = wp_get_attachment_image_src( $image_id, apply_filters( 'crate_hero_image_mobile_size', 'width=800&height=9999&crop=1' ) );

?>

<div class="hero" data-image="<?php echo esc_attr( $image[0] ); ?>" data-mobile-image="<?php echo esc_attr( $mobile_image[0] ); ?>" style="background-image: url('<?php echo esc_attr( $image[0] ); ?>');">

	<div class="hero-overlay-container">

		<h2><?php echo esc_html( get_sub_field( 'hero_heading' ) ); ?></h2>

		<?php the_sub_field( 'hero_content' ); ?>

		<?php if ( get_sub_field( 'hero_button_text' ) ) : ?>
			<a class="button" href="<?php echo esc_url( get_sub_field( 'hero_button_url' ) ); ?>"><?php echo esc_html( get_sub_field( 'hero_button_text' ) ); ?></a>
		<?php endif; ?>

	</div>

</div>
