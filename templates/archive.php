<?php
/**
 * The main archive template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Crate
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<header>
			<h1 class="page-title screen-reader-text"><?php the_archive_title(); ?></h1>
		</header>

		<?php if ( is_tax() || is_category() || is_tag() ) : ?>

		<aside class="term-meta">

			<div>
				Description: <?php echo term_description(); ?>
				<?php // Note: term_description will pull from the ACF term_description field IFF the native WP description field value is empty and there's a value in the ACF field. Otherwise it'll output the default, which is actually stored in the wp_term_taxonomy table ?>
			</div>

			<div>
				Featured Image ID: <?php the_field( 'term_featured_image' , 'term_' . get_queried_object_id() ); ?>
			</div>

			<div>
				Term Icon: <div style="display: inline-block;" class="icon-<?php the_field( 'term_icon' , 'term_' . get_queried_object_id() ); ?>"></div> <?php the_field( 'term_icon' , 'term_' . get_queried_object_id() ); ?>
			</div>

			<div>
				Term Color: <?php the_field( 'term_color' , 'term_' . get_queried_object_id() ); ?>
			</div>

		</aside>

		<?php endif; ?>

		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'loop' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();