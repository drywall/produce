<?php
/**
 * The template for displaying a single 'article' post.
 *
 * This may never get loaded if article_url is present and CRATE_ARTICLE_URL_HANDLER is
 * set to anything other than 'none' in inc/post-type-articles.inc
 *
 * @package Produce
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

				<div class="entry-meta">

					<?php // Output the article's type, linked to the archive of the type ?>
					<?php if ( ! empty( crate_get_article_type() ) ) : ?>
						<span class="article-type">
							<a href="<?php echo get_term_link( crate_get_article_type(), 'article-type' ); ?>"><?php crate_article_type(); ?></a>
						</span>
					<?php endif; ?>

					<?php // Output the article author and source, if present ?>
					<?php if ( ! empty( get_field( 'article_author' ) . get_field( 'article_source' ) ) ) : ?>
						<span class="article-attribution">

							<?php if ( ! empty( get_field( 'article_author' ) ) ): ?>
								By <?php the_field( 'article_author' ); if ( ! empty( get_field( 'article_source' ) ) ) echo ", "; ?>
							<?php endif; ?>

							<?php if ( ! empty( get_field( 'article_source' ) ) ): ?>
								<?php the_field( 'article_source' ); ?>
							<?php endif; ?>

						</span>
					<?php endif; ?>

					<?php // Output the article URL, if present ?>
					<?php if ( ! empty( get_field( 'article_url' ) ) ) : ?>
						<span class="article-link">
							<a href="<?php echo esc_url( get_field( 'article_url' ) ); ?>" target="_blank" rel="noopener noreferrer">View Original »</a>
						</span>
					<?php endif; ?>

				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->

			<div class="entry-content">

				<?php
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'crate' ), array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<?php // stuff in here ?>
			</footer><!-- .entry-footer -->
		</article><!-- #post-## -->

		<?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();